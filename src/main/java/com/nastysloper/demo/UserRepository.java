package com.nastysloper.demo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

/*
 *  This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository.
 */
@Service
public interface UserRepository extends CrudRepository<User, Long> {
}
