package com.nastysloper.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/demo")
public class UserController {

    private UserRepository userRepository;

    @Autowired
    public UserController(UserRepository repository) {
        this.userRepository = repository;
    }

    @GetMapping("/all")
    public @ResponseBody Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }

    @PostMapping("/add")
    public @ResponseBody String addUser(@RequestParam String name, @RequestParam String email) {
        User user = new User();
        user.setName(name);
        user.setEmail(email);
        userRepository.save(user);
        return "Saved";
    }

    @DeleteMapping("/remove")
    public @ResponseBody String deleteUser(@RequestParam Long id) {
        userRepository.deleteById(id);
        return "Removed";
    }

    @PatchMapping("/patch")
    public @ResponseBody String patchUser(@RequestBody User user) {
        User originalUser = userRepository.findById(user.getId()).get();
        String name = user.getName() == null ? originalUser.getName() : user.getName();
        String email = user.getEmail() == null ? originalUser.getEmail() : user.getEmail();

        user.setName(name);
        user.setEmail(email);
        userRepository.save(user);
        return "Patched";
    }

    @PutMapping("/update")
    public @ResponseBody String updateUser(@RequestBody User user) {
        userRepository.save(user);
        return "Updated";
    }
}
